# Pre-requisites

- NodeJS &geqslant; v8
- MongoDB &geqslant; v4.2.0

# Deployment Instructions

- `git clone git@gitlab.com:shscs911/travelmate.git`
- `cd travelmate`
- `sudo systemctl start mongodb.service`
- `npm install`
- `npm start`
- In your browser go to [http://127.0.0.1:7000/tourist](http://127.0.0.1:7000/tourist)
- Fill the data fields
- Click `Submit` in the top-right corner to submit the data
- Click `View All` in the top-right corner to view the list of touristplaces
- Click on the blue `profile` icon to view full details of the touristplace

# Demo Application

- [Travelmate](https://travelsign.herokuapp.com/tourist) - Deployed in Heroku
